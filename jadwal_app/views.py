from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .form import CustomerForm
from django.http import HttpResponseRedirect
from .models import Customer
from django.views.generic import ListView

def jadwal(request):
    form = CustomerForm()
    if request.method == 'POST':
        print(request.POST)
        form = CustomerForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/jadwal')

    context = {'form':form}
    return render(request, 'jadwal.html', context)

class JadwalView(ListView):
    model = Customer
    template_name = 'tampilan_jadwal.html'

def JadwalDetailView(request, id):
    mata_kuliah = Customer.objects.get(pk=id)
    return render(request, 'detail_jadwal.html', {"mata_kuliah":mata_kuliah})

def DeleteJadwalView(request, id):
    mata_kuliah = get_object_or_404(Customer, pk = id)
    mata_kuliah.delete()
    context = {
        "mata_kuliah": mata_kuliah
    }
    return render(request, 'delete_jadwal.html', context)