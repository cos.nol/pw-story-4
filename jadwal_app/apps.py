from django.apps import AppConfig


class JadwalAppConfig(AppConfig):
    name = 'jadwal_app'
