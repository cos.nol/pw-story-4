from django.db import models

class Customer(models.Model):
    nama_mata_kuliah = models.CharField(max_length = 200)
    dosen_pengajar = models.CharField(max_length = 200)
    jumlah_sks = models.IntegerField()
    deskripsi_mata_kuliah = models.CharField(max_length = 200)
    semester_tahun = models.CharField(max_length = 200)
    ruang_kelas = models.CharField(max_length = 200)

    def __str__(self):
        return self.nama_mata_kuliah + ' ' + self.dosen_pengajar

    
