from django.contrib import admin
from django.urls import include, path
from . import views
from .models import Customer
from .views import JadwalView, JadwalDetailView, DeleteJadwalView

app_name = 'jadwal_app'

urlpatterns = [
    path('jadwal/', views.jadwal, name='jadwal'),
    path('tampilan-jadwal/', JadwalView.as_view(), name='tampilan_jadwal'),
    path('detail-jadwal/<int:id>/', views.JadwalDetailView, name = 'detail_jadwal'),
    path('detail-jadwal/<int:id>/remove', views.DeleteJadwalView, name='delete_jadwal'),
]
