from django.test import TestCase, Client
from django.urls import reverse, resolve

from .models import Customer
from .views import  jadwal, JadwalDetailView, DeleteJadwalView, JadwalView
from .urls import *


class TestJadwalApp(TestCase):
    def test_jadwal_url(self):
        response = Client().get('/jadwal/')
        self.assertEquals(200, response.status_code)

    def test_tampilanjadwal_url(self):
        response = Client().get('/tampilan-jadwal/')
        self.assertEquals(200, response.status_code)

    def test_views_jadwal(self):
        found = resolve('/jadwal/')            
        self.assertEqual(found.func, jadwal)
