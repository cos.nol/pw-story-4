from django.test import TestCase, Client, RequestFactory 
from django.urls import reverse, resolve
from story_9.views import loginUser, signup, logoutUser
from story_9.forms import CreateUserForm
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.models import User

class TestStory8(TestCase):
    def test_login_view(self):
        response = self.client.post('/story-9/login/', {'username':'anaelsa', 'password' : 'tutifruti'})
        self.assertEqual(response.status_code, 200)

    def test_form_contribute_view(self):
        uname = get_user_model().objects.create_user(username = 'anaelsa')
        response = Client().post('/story-9/signup', data = {'username':'anaelsa', 'email' : 'tutifruti@gmail.com', 'password1':'tutifruti', 'password2':'tutifruti'})
        amount = User.objects.filter(username = "anaelsa").count()
        self.assertEqual(amount, 1)

    def test_logout(self):
        # Log in
        self.client.login(username='XXX', password="XXX")
        # Check response code
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)
        # Log out
        self.client.logout()
        # Check response code
        response = self.client.get('/story-9/logout/')
        self.assertEquals(response.status_code, 302)

