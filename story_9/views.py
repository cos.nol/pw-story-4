from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from story_9.forms import CreateUserForm
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

def loginUser(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username = username, password = password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            messages.info(request, 'Username ATAU Password salah')

    response = {}
    return render(request, 'login.html', response)

def signup(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/story-9/login/')

    response = {'form':form}
    return render(request, 'signup.html', response)

def logoutUser(request):
    logout(request)
    return HttpResponseRedirect('/story-9/login/')
