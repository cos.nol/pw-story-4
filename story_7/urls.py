from django.urls import path, include

from . import views

app_name = 'story_7'

urlpatterns = [
    path('story-7/', views.story_7, name='story_7'),

]

