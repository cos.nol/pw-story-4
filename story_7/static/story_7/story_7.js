$(document).ready(function(){
    $( function() {
    $( "#accordion" )
      .accordion({
        header: "> div > h3"
      })
    } );
  
    var selected=0;
    // $(".accordion-content").hide();

    
    var itemlist = $('#accordion');
    var len=$(itemlist).children().length; 
    
    $("#accordion div").click(function(){
        selected= $(this).index();
    });
      
  
      $(".up").click(function(e){
        e.preventDefault();
        if(selected>0)
        {
          jQuery($(itemlist).children().eq(selected-1)).before(jQuery($(itemlist).children().eq(selected)));
          selected=selected-1;
          
      }
    });
  
      $(".down").click(function(e){
          e.preventDefault();
        if(selected < len)
        {
          jQuery($(itemlist).children().eq(selected+1)).after(jQuery($(itemlist).children().eq(selected)));
          selected=selected+1;
      }
    });
      
  });