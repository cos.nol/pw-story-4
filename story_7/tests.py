from django.test import TestCase, Client
from django.urls import reverse, resolve

class TestStory7(TestCase):
    def test_story_7_url(self):
        response = Client().get('/story-7/')
        self.assertEquals(200, response.status_code)