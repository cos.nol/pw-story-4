from django.contrib import admin

from .models import ListKegiatan, FormKegiatan

admin.site.register(ListKegiatan)
admin.site.register(FormKegiatan)