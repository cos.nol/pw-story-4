from django.contrib import admin
from django.urls import include, path
from . import views
from .models import FormKegiatan, ListKegiatan
from .views import  DaftarKegiatanView, DaftarNamaOrangView, ListKegiatanView, ListNamaView

app_name = 'kegiatan_app'

urlpatterns = [
    path('daftarkan-kegiatan/', views.DaftarKegiatanView, name='daftarkegiatan'),
    path('list-kegiatan/', ListKegiatanView.as_view(), name='listkegiatan'),
    path('daftarkan-nama/', views.DaftarNamaOrangView, name='daftarnama'),
    path('list-nama/', ListNamaView.as_view(), name='listnama'),
]
