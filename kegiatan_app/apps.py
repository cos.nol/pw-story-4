from django.apps import AppConfig


class KegiatanAppConfig(AppConfig):
    name = 'kegiatan_app'
