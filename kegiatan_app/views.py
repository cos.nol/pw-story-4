from django.shortcuts import render
from django.views.generic import ListView
from django.http import HttpResponseRedirect
from .models import FormKegiatan, ListKegiatan
from .form import DaftarKegiatanForm, NamaOrangForm


def DaftarKegiatanView(request): #mendaftarkan kegiatan
    form = DaftarKegiatanForm()
    if request.method == 'POST':
        form = DaftarKegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/daftarkan-kegiatan')

    context = {'form':form}
    return render(request, 'tambah_kegiatan.html', context)

class ListKegiatanView(ListView): #menampilkan list kegiatan
    model = FormKegiatan
    template_name = 'list_kegiatan.html'

def DaftarNamaOrangView(request): #mendaftarkan nama ke kegiatan
    form = NamaOrangForm()
    if request.method == 'POST':
        form = NamaOrangForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/daftarkan-nama')

    context = {'form':form}
    return render(request, 'tambah_nama_orang.html', context)

class ListNamaView(ListView): #menampilkan list nama-nama
    model = FormKegiatan
    template_name = 'list_nama.html'
