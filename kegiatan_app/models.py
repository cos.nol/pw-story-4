from django.db import models

class FormKegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length = 200)

    def __str__(self):
        return self.nama_kegiatan

class ListKegiatan(models.Model):
    nama_kegiatan = models.ForeignKey(FormKegiatan, null = True, on_delete = models.CASCADE)
    nama_orang = models.CharField(max_length = 200)
    def __str__(self):
        return self.nama_orang
