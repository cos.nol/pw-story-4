from django.test import TestCase, Client
from django.urls import reverse, resolve

from .models import FormKegiatan, ListKegiatan
from .views import  DaftarKegiatanView, DaftarNamaOrangView, ListKegiatanView, ListNamaView
from .urls import *

class TestKegiatanApp(TestCase):

# Test untuk Views
    def test_views_daftarkegiatan(self):
        found = resolve('/daftarkan-kegiatan/')            
        self.assertEqual(found.func, DaftarKegiatanView)

    def test_views_daftarnama(self):
        found = resolve('/daftarkan-nama/')            
        self.assertEqual(found.func,DaftarNamaOrangView)


# Test untuk URL
    def test_daftarkan_kegiatan_url(self):
        response = Client().get('/daftarkan-kegiatan/')
        self.assertEquals(200, response.status_code)

    def test_daftarkan_nama_url(self):
        response = Client().get('/daftarkan-nama/')
        self.assertEquals(200, response.status_code)

    def test_list_kegiatan_url(self):
        response = Client().get('/list-kegiatan/')
        self.assertEquals(200, response.status_code)

    def test_list_nama_url(self):
        response = Client().get('/list-nama/')
        self.assertEquals(200, response.status_code)



    

