from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.


def story_8(request):
    response = {}
    return render(request, 'story_8.html', response)

def fungsi_data(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe = False)