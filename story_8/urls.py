from django.urls import path, include

from . import views

app_name = 'story_8'

urlpatterns = [
    path('story-8/', views.story_8, name='story_8'),
    path('data/', views.fungsi_data),
]

