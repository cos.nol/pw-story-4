from django.urls import path, include

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('tambahan/', views.tambahan, name='tambahan'),
    path('story1/', views.story1, name='story1'),
]

