from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def tambahan(request):
    return render(request, 'main/tambahan.html')

def story1(request):
    return render(request, 'main/story1.html')
