"""Story4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls', namespace='main')),
    path('', include('jadwal_app.urls', namespace='jadwal_app')),
    path('', include('kegiatan_app.urls', namespace='kegiatan_app')),
    path('', include('story_7.urls', namespace='story_7')),
    path('', include('story_8.urls', namespace='story_8')),
    path('story-9/', include('story_9.urls', namespace='story_9')),
]
